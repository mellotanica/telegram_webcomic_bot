package scrapers

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/mmcdole/gofeed"
	"strings"
)

func ScrapeDilbert(item *gofeed.Item, src string) ([]ComicUpdate, error) {
	gq, err := goquery.NewDocument(item.Link)
	if err != nil {
		return nil, err
	}

	comic, ok := gq.Find(".img-comic").First().Attr("src")
	if !ok {
		comic = item.Link
	}

	// src can have implicit schema
	if strings.HasPrefix(comic, "//") {
		schema := item.Link[:strings.Index(item.Link, "://")+1]
		comic = schema + comic
	}

	title := gq.Find(".comic-title-name").First().Text()
	if len(title) <= 0 {
		title = item.Title
	}

	c := make([]ComicUpdate, 1)
	c[0] = ComicUpdate{
		Source: src,
		Title:  title,
		Url:    comic,
	}
	return c, nil
}

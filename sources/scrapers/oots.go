package scrapers

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/mmcdole/gofeed"
	"github.com/pkg/errors"
	"gitlab.com/mellotanica/telegram_webcomic_bot/configs"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// OotS feed has only the global lastBuildDate tag, so the items must be kept in order
// using the comic number, to check if we are up to date, we check the comic number and store
// that additional info.

func TimeParserOots(item *gofeed.Item, feed *gofeed.Feed, src string) (time.Time, string, error) {
	conf := configs.GetConfigs()

	item_num := item.Title[:strings.Index(item.Title, ":")]
	item_i, err := strconv.Atoi(item_num)
	if err != nil {
		return time.Time{}, "", err
	}

	last_item, ok := conf.GetLastItem(src)
	if ok {
		last_i, err := strconv.Atoi(last_item)
		if err != nil {
			return time.Time{}, "", err
		}

		ok = last_i >= item_i
	}

	if !ok {
		t := time.Date(1970, 01, 01, 12, 10, 0, 0, time.UTC)
		// add the comic number as microseconds to sort them correctly if more than one comic
		// has to be sent (the motifyNewSources function sorts the items based on update time)
		t = t.Add(time.Duration(item_i) * time.Microsecond)
		return t, item_num, nil
	} else {
		t, ok := conf.GetFeed(src)
		if !ok {
			t = time.Time{}
		}
		return t, item_num, nil
	}
}

func ScrapeOots(item *gofeed.Item, src string) ([]ComicUpdate, error) {
	gq, err := goquery.NewDocument(item.Link)
	if err != nil {
		return nil, err
	}

	image := ""
	gq.Find("td > img").Each(func(i int, selection *goquery.Selection) {
		if len(image) <= 0 {
			img, ok := selection.Attr("src")
			if !ok {
				return
			}

			u, err := url.Parse(img)
			if err != nil {
				return
			}

			p := strings.TrimLeft(u.Path, "/")

			if strings.HasPrefix(p, "comics/oots/") || strings.HasPrefix(p, "comics/images/") {
				image = img
			}
		}
	})

	if len(image) <= 0 {
		return nil, errors.New("image link not found!")
	}

	p, err := url.Parse(image)
	if len(p.Host) <= 0 {
		u, err := url.Parse(item.Link)
		if err != nil {
			return nil, err
		}

		p.Host = u.Host
	}

	image = p.String()

	c := make([]ComicUpdate, 1)
	c[0] = ComicUpdate{
		Source: src,
		Title:  item.Title,
		Url:    image,
	}

	return c, nil
}

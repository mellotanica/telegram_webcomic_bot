package bot

import (
	"gitlab.com/mellotanica/telegram_webcomic_bot/sources"
	"gopkg.in/tucnak/telebot.v2"
)

func StartTasks(bot *telebot.Bot) {

	go sources.KeepFeedsUpdated(bot)

}
